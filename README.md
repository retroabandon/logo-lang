Microcomputer Logo Programming Language Implementations
=======================================================

This repository contains various implementations of and reverse
engineering work on (mostly 8-bit) microcomputer implementations of the
[Logo programming language][wp].

### Implementations

- `msx-lcsi`/: _MSX-Logo._ Cartridge-based. Developed by LCSI and released
  first by them and then in localised versions by others, particularly
  Philips in Europe and Yamaha in Japan.

### Developers/Publishers/Vendors

- __LCSI__ (Logo Computer Systems Inc.) did quite a number of
  implementations, including Apple II, Atari 8-bit, Mac and IBM PC, and
  went on to produce the [MicroWorlds][mw] system which embedded Logo
  within a general learning environment.



<!-------------------------------------------------------------------->
[wp]: https://en.wikipedia.org/wiki/Logo_(programming_language)
[mw]: https://en.wikipedia.org/wiki/MicroWorlds
