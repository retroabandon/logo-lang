MSX-Logo (LCSI)
===============

The [Generation-MSX MSX-Logo releases page][rel] has 11 different published
versions, all 32K ROM cartridges and requiring 32K RAM.
- Promotic International (Japan, 1986)
- Philips (Netherlands, U.K., France, Spain, Germany, Italy)
- Yamaha (Japan)
- Telemática/Talent (Argentina, 1986)
- COMtech, _COMtech LOGO_ (Poland, 1987)
- Logo Centrum Ede (Netherlands)


Sources:
- <https://download.file-hunter.com/Program%20language/>
- <https://www.msxblog.es/msx-logo/>



<!-------------------------------------------------------------------->
[rel]: https://www.generation-msx.nl/software/lcsi/msx-logo/release/2568/
